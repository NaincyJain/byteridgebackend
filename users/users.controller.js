﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/audit', auditUser);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;


function authenticate(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if(bearerHeader == undefined){
        userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or is incorrect' }))
        .catch(err => next(err));
    }else{
        userService.logout(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
    }
    
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => 
            {
                console.log(err)
                next(err)});
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function auditUser(req, res, next){
    const bearerHeader = req.headers['authorization'];
    userService.auditUser(bearerHeader)
    .then(users => users.length > 0 ? res.json(users): res.status(401).json({message:"You are not authorized for this data"}))
    .catch(err => next(err));
}